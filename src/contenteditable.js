const KEYCODE_ENTER = 13;
const KEYCODE_TAB = 9;
const KEYCODE_DELETE = 46;
const KEYCODE_BACKSPACE = 8;
const KEYCODE_CURSOR_LEFT = 37;
const KEYCODE_CURSOR_UP = 38;
const KEYCODE_CURSOR_RIGHT = 39;
const KEYCODE_CURSOR_DOWN = 40;

if (document.readyState !== 'loading') {
	initEditableFuncionality();
} else {
	document.addEventListener('DOMContentLoaded', initEditableFuncionality);
}

function initEditableFuncionality() {
	enableEditingLastCard();
	showEditableHint();
}

function enableEditingLastCard() {
	const lastCard = document.querySelector('.editable');

	lastCard.contentEditable = true;
	lastCard.addEventListener('input', autoSetDatasetValue);
	lastCard.addEventListener('keydown', autoPrevent);
	lastCard.addEventListener('focus', autoSelectText);
	lastCard.addEventListener('blur', ensureValue);
}

function showEditableHint() {
	document.querySelector('.card__hint').hidden = false;
}

function autoSetDatasetValue({ target }) {
	target.dataset.value = target.textContent;
};

function autoPrevent(e) {
	if ([KEYCODE_ENTER, KEYCODE_TAB].includes(e.keyCode)) e.preventDefault();
	if (![
		KEYCODE_DELETE,
		KEYCODE_BACKSPACE,
		KEYCODE_CURSOR_LEFT,
		KEYCODE_CURSOR_UP,
		KEYCODE_CURSOR_RIGHT,
		KEYCODE_CURSOR_DOWN
	].includes(e.keyCode) && e.target.textContent.length > 2) e.preventDefault();
};

function autoSelectText({ target }) {
	const range = document.createRange();
	range.selectNodeContents(target);

    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
}

function ensureValue({ target }) {
	target.textContent = target.textContent || String.fromCharCode(160);
}
