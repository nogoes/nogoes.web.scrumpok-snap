window.addEventListener('scroll', blur);
window.addEventListener('touchmove', blur);

if (document.readyState !== 'loading') {
	addFocusEventListeners();
} else {
	document.addEventListener('DOMContentLoaded', addFocusEventListeners);
}

function addFocusEventListeners() {
	document.querySelectorAll('.card').forEach(card => card.addEventListener('focus', focusCard));
}

function focusCard() {
	setTimeout(() => document.addEventListener('click', blur));
}

function blur() {
	document.removeEventListener('click', blur);

	document.activeElement.blur()
}