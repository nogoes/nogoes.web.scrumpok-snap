const version = 20191119;
const offlinefiles = [
	'/',
	'nogoes.css',
	'index.html',
	'contenteditable.js',
	'focus.js'
];

self.addEventListener('install', event => {
	event.waitUntil(
		caches.open(`swoffline${version}`).then(cache => cache.addAll(offlinefiles))
	);
});

self.addEventListener('fetch', event => {
	if (event.request.method !== 'GET') return;

	event.respondWith(
		caches.match(event.request).then(cachedResponse => {
			return cachedResponse || fetch(event.request).then(response => {
				const clone = response.clone();

				cacheResponse(event, clone);

				return response;
			})
		})
	);

	function cacheResponse(event, response) {
		if (response.status != 200) return;

		caches.open(`swcache${version}`).then(cache => {
			cache.put(event.request, response.clone());
		});
	}
});

self.addEventListener('activate', event => {
	event.waitUntil(
		caches.keys().then(keys => {
			keys.filter(key => !key.endsWith(version)).forEach(key => caches.delete(key))
		})
	);
});
