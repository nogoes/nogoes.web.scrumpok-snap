const { src, dest, series, parallel } = require('gulp');

const terser = require('gulp-terser');
const imagemin = require('gulp-imagemin');
const htmlnano = require('gulp-htmlnano');
const cleancss = require('gulp-clean-css');
const replace = require('gulp-replace');
const del = require('del');
const fs = require('fs');

const source = 'src/';
const destination = 'dist/';

function clearDestination() {
	return del([`${destination}**/*`]);
}

function copyMetadatas() {
	return src([
		`${source}*.webmanifest`,
		`${source}*.xml`
	]).pipe(dest(destination));
}

function copyFavicons() {
	return src([
		`${source}*.png`,
		`${source}*.ico`,
		`${source}*.svg`
	])
	.pipe(dest(destination));
}

function buildJs() {
	return src([
		`${source}*.js`
	])
	.pipe(terser())
	.pipe(dest(destination))
}

function buildCss() {
	return src([
		`${source}*.css`
	])
	.pipe(cleancss())
	.pipe(dest(destination))
}

function buildHtml() {
	return src([
		`${source}*.html`
	])
	.pipe(replace('<!-- version -->', function() {
		const packagejson = fs.readFileSync('package.json', { encoding: 'utf8' });
		const version = JSON.parse(packagejson).version;

		return `<meta name="version" content="${version}">`;
	}))
	.pipe(htmlnano({
		minifySvg: false
	}))
	.pipe(dest(destination))
}

exports.default = series(clearDestination, parallel(buildJs, buildCss, buildHtml, copyFavicons, copyMetadatas));